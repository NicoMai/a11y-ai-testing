# Test der a11y-ai für Developer

## Vorgehen
Es wird das bad html Beispiel der W3C importiert
### Fehlerzahl vor der Korrektur auf den HTML Seiten mit lighthouse und axe

| Datei | Lighthouse vorher | webAxe vorher | Siteimprove vorher | Lighthouse nachher | webAxe nachher | Siteimprove nachher | Lighthouse Quote | webAxe Quote | Siteimprove Quote |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| home | 58 | 67 | 9 issues with 73 |  |  |  |  |  |  |
| news | 66 | 52 | 9 issues with 81 |  |  |  |  |  |  |
| tickets | 56 | 48 | 11 issues with 56 |  |  |  |  |  |  |
| survey | 58 | 63 | 12 issues with 80 |  |  |  |  |  |  |

### Zeiten der Korrektur auf einem Mac Mini M1

Zeiten sind egal, da der Server rechnet und nicht der Client. Wird also übeall ähnlich langsam sein.

### Fehler exportieren zum Vergleich
lighthouse und webAxe abgelegt

### Vergleich der Dateigrößen vorher und nachher

| Datei | Zeilen vorher | Zeichen vorher | Bytes vorher | Zeilen nachher | Zeichen nachher | Bytes nachher | Zeilen Quote | Bytes Quote | Zeichen Quote |
 --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| home | 371 | 22922 | 23292 | 597 | 24823 |  | 161% | 108% |  |
| news | 280 | 23151 | 23430 | 402 | 26631 |  | 144% | 115% |  |
| tickets | 299 | 22017 | 22315 | 650 | 26700 |  | 217% | 121% |  |
| survey | 600 | 40242 | 40841 | 863 | 34634 |  | 144% | 86% |  |

### Wie viele Durchläufe braucht es bis keine Fehler mehr gefunden sind

| Iteration | Datei | Zeit | Diff | Score Lighthouse | Score webAxe | Score Siteimprove | Zeilen | Bytes | Zeichen |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| 1 | home |  | ja |  |  |  |  |  |  |
| 2 | home |  | ja |  |  |  |  |  |  |
| 3 | home |  | ja |  |  |  |  |  |  |
| 4 | home |  |  |  |  |  |  |  |  |
| 5 | home |  |  |  |  |  |  |  |  |
| 6 | home |  |  |  |  |  |  |  |  |
| 7 | home |  |  |  |  |  |  |  |  |

## Werden die guten Seiten auch korrigiert?

| Datei | Zeilen vorher | Zeichen vorher | Bytes vorher | Zeilen nachher | Zeichen nachher | Bytes nachher | Zeilen Quote | Zeichen Quote | Bytes Quote |
 --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| home | 130 | 10700 | 10960 | 150 | 11015 | 11165 | 115% | 103% | 102% |
| news | 138 | 12755 | 13031 | 166 | 12632 | 12798 | 120% | 99% | 98% |
| tickets | 198 | 14365 | 14761 | 267 | 14553 | 14820 | 135% | 101% | 100% |
| survey | 417 | 27360 | 28194 | 516 | 24263 | 24779 | 124% | 89% | 88% |